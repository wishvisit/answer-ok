<?php
/**
 * Template Name:Page Default
 *
 * @package C-point
 * @subpackage Answer
 * @since 1.0.0
 */
?>
<?php get_template_part( 'header' ); ?>
<div class="site-content">
	<div class="container-fluid" id="article-title">
		<div class="row" >
			<div class="col">
				<h2><?php the_title();?></h2>
			</div>
		</div>
		</div>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-9 ">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
					<div class="row">
						<div class="col">
							<h3 class="content-title"><?php the_title();?></h3>
						</div>
					</div>
					<div class="row article-content">
						<div class="col">
							<?php the_content();?>
						</div>
					</div>
					<div class="row article-contact bar-link">
						<div class="col ">
							<?php if(pll_current_language() == 'en') :?>
								<a href="mailto:info@c-point.co.th" id="contacten"></a>
							<?php elseif(pll_current_language() == 'th') :?>
								<a href="mailto:info@c-point.co.th" id="contactth"></a>
							<?php endif;?>
					
						</div>
					</div>
					
					<?php endwhile; else: ?>
				<p>Sorry, no posts matched your criteria.</p>
				<?php endif; ?>
			</div>
			<div class="col-md-3 d-none d-md-block sidebar">
			<?php if ( is_active_sidebar( 'home_sidebar' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'home_sidebar' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
			</div>
		</div>
	</div><?php /*
	<div class="container" id="relate-post">
		<div class="row">
			<div class="col">
				<h3>Other Article</h3>
			</div>
		</div class="row">		
			<div class="col" >
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/dummy-relate.png" alt="homeslide"/>
			</div>
		</div>
	</div>
	*/?>
</div>
<section id="post-footer">
	<div class="row">
		<div class="col">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/post-footer-logo.png" alt="footer logo"/>
		</div>
	</div>
</section>
<?php wp_footer();?>
</body>
</html>