<?php
/**
 * The Thai homepage
 * Template Name: HomeTh
 * @package C-point
 * @subpackage Answer
 * @since 1.0.0
 */
?>
<?php get_template_part( 'header' ); ?>
<div class="site-content">
	<section id="homepage-slide" class="container">
		<div class="row">
			<div class="col slide-container">
				<div id="slideth" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#slideth" data-slide-to="0" class="active"></li>
						<li data-target="#slideth" data-slide-to="1"></li>
						<li data-target="#slideth" data-slide-to="2"></li>
					</ol>
				<div class="carousel-inner">
					<div class="carousel-item active" data-interval="3000">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-slide1.png" alt="homeslide"/>
					</div>
					<div class="carousel-item" data-interval="3000">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-slide2.png" alt="homeslide"/>
					</div>
					<div class="carousel-item" data-interval="3000">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-slide3.png" alt="homeslide"/>
					</div>
				</div>
				<a class="carousel-control-prev" href="#slideth" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#slideth" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				</div>
			</div>
		</div>
	</section>
	<section id="home-afterslide" class="container">
		<div class="row">
			<div class="col left-banner homebanner">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-banner1.png" alt="homeslide"/>
				<h4>วิสัยทัศน์</h4>
				<p class="font-weight-bold">คุณภาพของวิสัยทัศที่นำไปสู่ความสำเร็จ</p>
				<p>DENTAL LABORATORY ANSWER ของเรา เพื่อสร้างความสุขในการรับประทานอาหารและฟันที่สวยงามของทุกคนให้กลับอีกครั้ง คติของเราคือการใส่ใจทุกรายละเอียดให้คนไข้รู้สึกเติมเต็มให้ฟันกลับมาสวยอีกครั้งด้วยเซรามิก 100%</p>
				<img class="bigbanner" src="<?php echo get_template_directory_uri(); ?>/assets/images/home-banner3.png" alt="homeslide"/>
			</div>
			<div class="col right-banner homebanner">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-banner2.png" alt="homeslide"/>
				<h4>การส่งต่อเทคนิค</h4>
				<p class="font-weight-bold">เรายังคงส่งมอบเทคนิคการผลิตตัวผลิตภัณฑ์ที่มีคุณภาพสูงต่อไป</p>
				<p>"DENTAL LABORATORY ANSWER ของเรามี 7 ผู้เชี่ยวชาญที่มีใบรับรองการศึกษาแห่งชาติ ด้วยระบบอันยอดเยี่ยมที่หาได้ยาก <br>เราจึงยังคงทำการผลิตตัวผลิตภัณฑ์และส่งต่อเทคนิคการผลิตสินค้าคุณภาพสูงต่อไป<br><br>
				<span class="font-weight-bold">"ช่างเทคนิคทันตกรรม" ผู้เชี่ยวชาญที่มีใบรับรองการศึกษาแห่งชาติของประเทศญี่ปุ่น</span><br>
				การทำงานในประเทศญี่ปุ่นจำเป็นต้องได้รับใบรับรองการศึกษาแห่งชาติของช่างเทคนิคทันตกรรม โดยจำเป็นต้องศึกษาความรู้ทางการแพทย์และทักษะทางทันตกรรมอย่างน้อย 2 ปี <br>
				หลังจบการศึกษาระดับมัธยมในสถาบันที่ได้รับใบอนุญาตระดับประเทศเพื่อเข้ารับการสอบใบรับรองการศึกษาแห่งชาติ"</p>
			</div>
		</div>
	</section>
	<section id="home-link" class="container">
		<div class="row">
			<div class="col">
				<a href="<?php echo home_url("/th/product/");?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-th.png" alt="Denture"/></a>
				<a href="<?php echo home_url("/th/enveroment/");?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-env-th.png" alt="preparation"/></a>
				<a href="<?php echo home_url("/th/order-2/");?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/order-th.png" alt="dentist equipment"/></a>
			</div>
		</div>
		<div class="row bar-link" >
			<div class="col right">
				<a href="<?php echo home_url("/th/about-us-2/");?>" id="companyth"></a>
			</div>
			<div class="col left">
				<a href="mailto:info@c-point.co.th" target="_blank" id="contactth"></a>
			</div>
		</div>
	</section>
	<section id="sitemap" class="container">
		<div class="row">
			<div class="col">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/site-map.png" alt="sitemap logo"/>
				<h4 style="color: white;">แผนที่</h4>
			</div>
		</div>
	</section>
	<section id="map" class="container">
		<div class="row">
			<div class="col">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3578.2303061712582!2d127.71996161503112!3d26.254187783415635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34e56b803c86dc83%3A0x3bf47dd59ae842d!2z77yI5pyJ77yJ44Ki44Oz44K144O8!5e0!3m2!1sth!2sth!4v1580894684150!5m2!1sth!2sth" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</section>
	<section id="home-footer" class="container">
		<div class="row">
			<div class="col">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-logo.png" alt="footer logo"/>
			</div>
		</div>
	</section>
</div>

<?php wp_footer();?>
</body>
</html>