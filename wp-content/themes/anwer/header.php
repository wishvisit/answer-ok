<?php
/**
 * Header file
 *
 * @package C-point
 * @subpackage Answer
 * @since 1.0.0
 */
?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Answer</title>
<link rel="icon" href="<?php echo get_template_directory_uri();?>/assets/images/favicon.png">
<?php wp_head();?>
</head>
<body>
<section id="site-header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3" id="site-logo">
				<a href="<?php echo home_url();?>" alt="home"><img src="<?php echo get_template_directory_uri();?>/assets/images/footer-logo.png" alt="answer logo"/></a>
			</div>
			<div class="col-xs-6 d-none col-md-9 d-sm-none d-md-block" id="site-menu">
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			</div>
			<div class="col-6 d-block d-sm-block d-md-none" id="site-menu">
				<button class="mobile-toggle-container navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
				aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">  
					<div id="menuToggle">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</button>
			</div>
		</div>
		<nav class="navbar navbar-light light-blue lighten-4 col-6 d-block d-sm-block d-md-none" id="mobile-menu">
			<div class="collapse navbar-collapse" id="navbarSupportedContent1">
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			</div>
		</nav>
	</div>
</section>

