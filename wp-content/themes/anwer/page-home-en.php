<?php
/**
 * The English homepage
 * Template Name: HomeEn
 * @package C-point
 * @subpackage Answer
 * @since 1.0.0
 */
?>
<?php get_template_part( 'header' ); ?>
<div class="site-content">
	<section id="homepage-slide" class="container">
		<div class="row">
			<div class="col slide-container">
				<div id="slideen" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#slideen" data-slide-to="0" class="active"></li>
						<li data-target="#slideen" data-slide-to="1"></li>
						<li data-target="#slideen" data-slide-to="2"></li>
					</ol>
				<div class="carousel-inner">
					<div class="carousel-item active" data-interval="3000">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-slide1.png" alt="homeslide"/>
					</div>
					<div class="carousel-item" data-interval="3000">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-slide2.png" alt="homeslide"/>
					</div>
					<div class="carousel-item" data-interval="3000">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-slide3.png" alt="homeslide"/>
					</div>
				</div>
				<a class="carousel-control-prev" href="#slideen" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#slideen" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				</div>
				
			</div>
		</div>
	</section>
	<section id="home-afterslide" class="container">
		<div class="row">
		<div class="col left-banner homebanner">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-banner1.png" alt="homeslide"/>
				<h4>Vision</h4>
				<p class="font-weight-bold">Product quality backed by actual results</p>
				<p>For a renewed joy in eating and a beautiful mouth that everybody desires, Dental Laboratory Answer recreates beautiful teeth made of all-ceramic materials, with the precept of a finish that feels comfortable to meticulous patients.</p>
				<img  class="bigbanner" src="<?php echo get_template_directory_uri(); ?>/assets/images/home-banner3.png" alt="homeslide"/>
			</div>
			<div class="col right-banner homebanner">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-banner2.png" alt="homeslide"/>
				<h4>Technological Transition</h4>
				<p class="font-weight-bold">Passing on technology and maintaining the highest quality</p>
				<p>"Dental Laboratory Answer has seven nationally, qualified specialists. With small numbers elite systems, we dedicatedly pass on technologies to maintain high product quality and we manufacture well-crafted products.<br><br>
				<span class="font-weight-bold">“Dental Technician”- Japan’s national qualification</span><br>
				To work as a dental technician in Japan, you need to obtain a national qualification as a dental technician, and in order to take the national qualification for a dental technician, you must study and acquire medical knowledge and dental skills at a nationally approved educational institution for more than two years after graduating from high school."</p>
			</div>
		</div>
	</section>
	<section id="home-link" class="container">
		<div class="row">
			<div class="col">
				<a href="<?php echo home_url("/productlist/");?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-en.png" alt="Denture"/></a>
				<a href="<?php echo home_url("/production-environment/");?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-env-en.png" alt="preparation"/></a>
				<a href="<?php echo home_url("/order/");?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/order-en.png" alt="dentist equipment"/></a>
			</div>
		</div>
		<div class="row bar-link" >
			<div class="col right">
				<a href="<?php echo home_url("/about-us/");?>" id="companyen"></a>
			</div>
			<div class="col left">
				<a href="mailto:info@c-point.co.th" target="_blank" id="contacten"></a>
			</div>
		</div>
	</section>
	<section id="sitemap" class="container">
		<div class="row">
			<div class="col">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/site-map.png" alt="sitemap logo"/>
				<h4 style="color: white;">MAP</h4>
			</div>
		</div>
	</section>
	<section id="map" class="container">
		<div class="row">
			<div class="col">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3578.2303061712582!2d127.71996161503112!3d26.254187783415635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34e56b803c86dc83%3A0x3bf47dd59ae842d!2z77yI5pyJ77yJ44Ki44Oz44K144O8!5e0!3m2!1sth!2sth!4v1580894684150!5m2!1sth!2sth" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
	</section>
	<section id="home-footer" class="container">
		<div class="row">
			<div class="col">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-logo.png" alt="footer logo"/>
			</div>
		</div>
	</section>
</div>

<?php wp_footer();?>
</body>
</html>