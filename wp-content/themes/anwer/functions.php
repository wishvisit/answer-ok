<?php
/**
 *
 *
 * @package C-point
 * @subpackage Answer
 * @since 1.0.0
 */

function boostrap_script(){
	wp_enqueue_script( 'boostrap_script',get_template_directory_uri().'/assets/js/bootstrap.bundle.min.js', array( 'jquery' ), null, true );	
	wp_enqueue_style( 'boostrap_script',get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), "1.0" );
	wp_enqueue_style( 'base',get_template_directory_uri().'/style.css', array(), "1.0" );
}
add_action( 'wp_enqueue_scripts', 'boostrap_script' ); 

add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images' );
// Remove default image sizes here. 
function prefix_remove_default_images( $sizes ) {
 unset( $sizes['small']); // 150px
 unset( $sizes['medium']); // 300px
 unset( $sizes['large']); // 1024px
 unset( $sizes['medium_large']); // 768px
 return $sizes;
}
// theme support

function register_everything() {
	register_nav_menus(
	  array(
		'primary' => __( 'Primary' ),
	  )
	);
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	add_theme_support( 'post-thumbnails' );
	if ( function_exists('register_sidebar') )
		register_sidebar(array(
			'name' 			=> 'Sidebar',
			'id'         	=> 'home_sidebar',
			'before_widget' => '<div class = "sidebar">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
		)
	);
  }
  add_action( 'init', 'register_everything' );